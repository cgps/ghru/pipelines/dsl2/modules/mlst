include { run_ariba; combine_mlst_reports } from './processes'
workflow run_mlst{
    take: reads

    main:
    mlst_reports = run_ariba(reads, params.species_db)
    collected_reports = mlst_reports.simple_reports.collect( sort: {a, b -> a[0].getBaseName() <=> b[0].getBaseName()} )
    combine_mlst_reports(collected_reports)
}